// Les listes de blocs
liste_unites = []
liste_dizaines = []
liste_centaines = []
liste_milliers = []
// Les listes de groupes de blocs (paquets de 10 qui ne sont pas encore déplacés dans la colonne des dizaines)
liste_groupes_unites = []
liste_groupes_dizaines = []
liste_groupes_centaines = []
liste_groupes_milliers = []
// Les listes temporaires de blocs sélectionnés par le rectangle bleu
unitesagrouper = []
dizainesagrouper = []
centainesagrouper = []
// Les types de blocs possibles
liste_types = ['unite', 'dizaine', 'centaine', 'millier']

// Emplilement de départ (z-index) qui ma monter d'un cran à chaque fois qu'on crée ou déplace un bloc
var hauteur = 1
var oeil = true;
var affiche_compteur = true;
var X;
var Y;
// Définit si les colonnes sont fusionnées
var fusion = false;
// Définit si le rideau (masque noir) est déployé
var rideau_on = false;

var deplacement = false;

let zoom_unites = 1;
let zoom_dizaines = 1;
let zoom_centaines = 1;
let zoom_milliers = 1;

// Réglages de départ
document.getElementById("col_2").checked = true;
document.getElementById("fusionner").checked = false;
document.getElementById("apropos").style.display = 'none';
document.getElementById("darkbox").style.display = 'none';
document.getElementById("canvas").width = document.getElementById('zone_bas').offsetWidth - 150;
document.getElementById("canvas").height = document.getElementById('zone_bas').offsetHeight - 50;
document.getElementById("nombre_choix").value = "";


// Affichage ou désactivation du compteur général
function visibilite_general() {
  if (oeil === true) {
    document.getElementById('compteur_general').style.display = 'none';
    document.getElementById('interrupteur_general').style.backgroundImage = 'url(images/oeil_inactif.png)';
    oeil = false;
  } else {
    if (affiche_compteur === true) { document.getElementById('compteur_general').style.display = 'block'; }
    document.getElementById('interrupteur_general').style.backgroundImage = 'url(images/oeil.png)';
    oeil = true;
  }
}

// On compte combien d'objets sont présents dans chaque liste et on multiplie par la bonne puissance de 10.
function maj_compteur_general() {
  document.getElementById("compteur_general").innerHTML = liste_unites.length + (10 * (liste_dizaines.length + liste_groupes_unites.length)) + (100 * (liste_centaines.length + liste_groupes_dizaines.length)) + (1000 * (liste_milliers.length + liste_groupes_centaines.length));
  verifie_compteurs();
}


// Dimensions des blocs

function dimensions(objet) {
  let largeur_objet;
  let hauteur_objet;
  let largeur_zone=document.getElementById('zone_unites').offsetWidth;
  let facteur;
  if (objet === "unite") {
    largeur_objet = 0.7 * (largeur_zone * 15) / 232;
    hauteur_objet = 0.7 * (largeur_zone * 15) / 232;
    facteur = zoom_unites;
  }
 
  else if (objet === "dizaine" || objet ==="groupe_unites") {
    largeur_objet = 0.7 * largeur_zone / 2;
    hauteur_objet = 0.7 * (largeur_zone * 15) / 232;
    facteur = zoom_dizaines;  
  }

  else if (objet === "centaine" || objet ==="groupe_dizaines") {
    largeur_objet = 0.7 * largeur_zone / 2;
    hauteur_objet = 0.7 * largeur_zone / 2;
    facteur = zoom_centaines;
  }

  else if (objet === "millier" || objet ==="groupe_centaines") {
    largeur_objet = 0.7 * largeur_zone * (5 / 8);
    hauteur_objet = 0.7 * largeur_zone * (5 / 8);
    facteur = zoom_milliers;
  }

  largeur_objet = largeur_objet*facteur;
  hauteur_objet = hauteur_objet*facteur;

  return [largeur_objet,hauteur_objet]
}


// Création d'un bloc
function cree(zone, objet, mode) {
  var div = document.getElementById(zone);
  var img = document.createElement("img");
  let dimensions_objet=dimensions(objet);
  let hauteur_objet = dimensions_objet[1];
  let largeur_objet = dimensions_objet[0];
  let liste_groupes;

  if (objet === "unite") {
    liste_objets = liste_unites
    type_compteur = "compteur_unites"
    liste_groupes = liste_groupes_unites
  }
  if (objet === "dizaine") {
    liste_objets = liste_dizaines
    type_compteur = "compteur_dizaines"
    liste_groupes = liste_groupes_dizaines
  }
  if (objet === "groupe_unites") {
    liste_objets = liste_unites
    type_compteur = "compteur_unites"
    liste_groupes = liste_groupes_unites
  }
  if (objet === "centaine") {
    liste_objets = liste_centaines
    type_compteur = "compteur_centaines"
    liste_groupes = liste_groupes_centaines
  }
  if (objet === "groupe_dizaines") {
    liste_objets = liste_dizaines
    type_compteur = "compteur_dizaines"
    liste_groupes = liste_groupes_dizaines
  }
  if (objet === "millier") {
    liste_objets = liste_milliers
    type_compteur = "compteur_milliers"
    liste_groupes = liste_groupes_milliers
  }
  if (objet === "groupe_centaines") {
    liste_objets = liste_centaines
    type_compteur = "compteur_centaines"
    liste_groupes = liste_groupes_centaines
  }
  if (objet === "groupe_unites") { img.src = `images/dizaine.svg`; }
  else if (objet === "groupe_dizaines") { img.src = `images/centaine.svg`; }
  else if (objet === "groupe_centaines") { img.src = `images/millier.svg`; }
  else { img.src = `images/${objet}.svg`; }
  img.style.position = "absolute";

  if (mode === 'auto') {
    if (fusion === false) {
      img.style.left = Math.floor(Math.random() * (div.offsetWidth - largeur_objet - 10)) + 5 + "px";
      img.style.top = Math.floor(Math.random() * (div.offsetHeight - hauteur_objet - 10)) + 5 + "px";
    } else {
      img.style.left = Math.floor(Math.random() * (document.getElementById('zone_globale').offsetWidth - largeur_objet - 10)) - div.offsetLeft + 155 + "px";
      img.style.top = Math.floor(Math.random() * (document.getElementById('zone_globale').offsetHeight - hauteur_objet - 10)) + 5 + "px";
    }
  }
  if (mode === 'casse') {
    position = position1_casse(objet);
    img.style.left = position[0] + 'px';
    img.style.top = position[1] + 'px';
  }
  if (mode === 'groupe') {
    position = position_groupe(objet);
    console.log('largeur_objet ' + largeur_objet)
    img.style.left = ((position[0]) - largeur_objet / 2) + 'px';
    img.style.top = ((position[1]) - hauteur_objet / 2) + 'px';
  }
  img.style.height = `${hauteur_objet}px`;
  img.style.zIndex = hauteur + 1;
  if (rideau_on === true) { img.style.display = 'none'; }
  hauteur++
  // img.classList.add("ombre-portee");
  img.classList.add("draggable");
  img.classList.add(objet);
  img.setAttribute("draggable", "false"); // évite l'image fantôme lors du drag
  div.appendChild(img);

  if (mode == 'groupe' && fusion === false) { liste_groupes.push(img); }
  else { liste_objets.push(img); }

  maj_compteurs()
  // document.getElementById(type_compteur).innerHTML = liste_objets.length+(liste_groupes.length*10);
  maj_compteur_general()
}

rang_casse = 0;

function moyenne(tableau) {
  var somme = tableau.reduce(function (a, b) {
    return a + b;
  });
  var moyenne = somme / tableau.length;
  return moyenne;
}



// Calcul de la position d'un groupe (ex:groupe de 10 unités)
function position_groupe(objet) {
  diffrectX = zone_unites.offsetLeft - zone_dizaines.offsetLeft;
  let posx;
  let posy;
  let listeObjets;
  let div = document.getElementById('zone_unites')
  let correctifx;
  let correctify;
  if (objet === 'groupe_unites' || objet === 'dizaine') { listeObjets = unitesagrouper; correctifx = 0.35 * (div.offsetWidth * 15) / 232; correctify = 0.35 * (div.offsetWidth * 15) / 232 }
  else if (objet === 'groupe_dizaines' || objet === 'centaine') { listeObjets = dizainesagrouper; correctifx = 0.7 * div.offsetWidth / 4; correctify = 0.35 * (div.offsetWidth * 15) / 232 }
  else if (objet === 'groupe_centaines' || objet === 'millier') { listeObjets = centainesagrouper; correctifx = 0.7 * div.offsetWidth / 4; correctify = 0.7 * div.offsetWidth / 4 }


  // créer un tableau pour stocker les valeurs de style.left
  const valeursLeft = [];
  const valeursTop = [];

  // boucler sur chaque objet de la liste et récupérer la valeur de style.left
  listeObjets.forEach(function (objet) {
    valeursLeft.push(parseInt(objet.style.left) + correctifx);
    valeursTop.push(parseInt(objet.style.top) + correctify);

  });

  // calculer la moyenne des valeurs de style.left
  let moyenneLeft = valeursLeft.reduce(function (total, valeur) {
    return total + valeur;
  }) / valeursLeft.length;
  const moyenneTop = valeursTop.reduce(function (total, valeur) {
    return total + valeur;
  }) / valeursTop.length;

  if (fusion === true) { moyenneLeft = moyenneLeft + diffrectX }

  return [moyenneLeft, moyenneTop]
}

// Calcul de la position des unités avant animation de cassage
function position1_casse(objet) {
  let posx;
  let posy;
  let correctif_y = 0;
  let correctif_x = 0;
  let zone_travail;
  let larg_reference = document.getElementById('zone_unites').offsetWidth
  if (fusion === false) { zone_travail = document.getElementById('zone_unites') } else { zone_travail = document.getElementById('zone_globale') }
  let larg_zone = zone_travail.offsetWidth
  let haut_zone = zone_travail.offsetHeight
  let dimensions_objet=dimensions(objet);
  let hauteur_objet = dimensions_objet[1];
  let largeur_objet = dimensions_objet[0];

  if (objet === 'unite') {
    let ecart_x = 0.53 * (larg_reference * 15) / 232
    let ecart_y = 0
    if (posXcasse + (ecart_x * 10) > larg_zone) { correctif_x = larg_zone - (posXcasse + ecart_x * 10) }
    posx = correctif_x + posXcasse + (ecart_x * rang_casse);
    posy = correctif_y + posYcasse + (ecart_y * rang_casse);;
  }
  if (objet === 'dizaine') {
    let ecart_x = 0
    let ecart_y = 0.53 * (larg_reference * 15) / 232
    if (posXcasse + (ecart_x * 10) > larg_zone) { correctif_x = larg_zone - (posXcasse + ecart_x * 10) }
    posx = correctif_x + posXcasse + (ecart_x * rang_casse);
    posy = correctif_y + posYcasse + (ecart_y * rang_casse);;
  }
  if (objet === 'centaine') {
    let ecart_x = 0.2 * (larg_reference * 15) / 232
    let ecart_y = 0.2 * (larg_reference * 15) / 232
    posx = correctif_x + posXcasse - (ecart_x * rang_casse);
    posy = correctif_y + posYcasse + (ecart_y * rang_casse);;
  }

  if (rang_casse < 9) { rang_casse++ } else { rang_casse = 0 }
  return [posx, posy]


}

// Calcul de la position des objets résultant d'un cassage.
function position_casse(objet) {
  let posx;
  let posy;
  let correctif_y = 0;
  let correctif_x = 0;
  let zone_travail;
  let larg_reference = document.getElementById('zone_unites').offsetWidth
  if (fusion === false) { zone_travail = document.getElementById('zone_unites') } else { zone_travail = document.getElementById('zone_globale') }
  let larg_zone = zone_travail.offsetWidth
  let haut_zone = zone_travail.offsetHeight

  if (liste_dizaines.includes(dragged) || liste_groupes_unites.includes(dragged)) {
    let ecart_x = 0.7 * (larg_reference * 15) / 232
    if (posXcasse + (ecart_x * 10) > larg_zone) { correctif_x = larg_zone - (posXcasse + ecart_x * 10) }
    posx = correctif_x + posXcasse + (ecart_x * rang_casse);
    posy = correctif_y + posYcasse;
  }

  if (liste_centaines.includes(dragged) || liste_groupes_dizaines.includes(dragged)) {
    let ecart_y = 0.7 * (larg_reference * 15) / 232
    if (posYcasse + (ecart_y * 10) > haut_zone) { correctif_y = haut_zone - (posYcasse + ecart_y * 10) }
    posx = correctif_x + posXcasse;
    posy = correctif_y + posYcasse + (ecart_y * rang_casse);
  }

  if (liste_milliers.includes(dragged) || liste_groupes_centaines.includes(dragged)) {
    let ecart_x = 0.4 * (larg_reference * 15) / 232
    let ecart_y = 0.4 * (larg_reference * 15) / 232

    if (fusion === false && posXcasse - (ecart_x * 10) < 0) { correctif_x = (ecart_x * 10) - posXcasse }
    else if (fusion === true && diffrectX + posXcasse - (ecart_x * 10) < 0) { correctif_x = (ecart_x * 10) - (posXcasse + diffrectX) }
    if ((0.7 * larg_reference / 2) + posYcasse + (ecart_y * 10) > haut_zone) { correctif_y = haut_zone - (posYcasse + ecart_y * 10) - (0.7 * larg_reference / 2) }

    posx = correctif_x + posXcasse - (ecart_x * rang_casse);
    posy = correctif_y + posYcasse + (ecart_y * rang_casse);
  }

  if (rang_casse < 9) { rang_casse++ } else { rang_casse = 0 }

  return [posx, posy]


}

// Mise à zéro d'une colonne (corbeille)
function reset(liste_objets, objet) {
  supprime(objet, liste_objets.length, 'auto')
}

// Mise à zéro globale (toutes les colonnes)
function reset_all() {
  reset(liste_milliers, 'millier')
  reset(liste_centaines, 'centaine')
  reset(liste_groupes_centaines, 'groupe_centaines')
  reset(liste_dizaines, 'dizaine')
  reset(liste_groupes_dizaines, 'groupe_dizaines')
  reset(liste_unites, 'unite')
  reset(liste_groupes_unites, 'groupe_unites')
}

// Suppression d'un bloc
function supprime(objet, nombre, mode) {

  let n = 0;

  if (objet === "unite") {
    liste_objets = liste_unites;
    type_compteur = "compteur_unites";
    listegroupe = unitesagrouper;
    liste_groupes = liste_groupes_unites;
  }
  if (objet === "groupe_unites") {
    liste_objets = liste_groupes_unites;
    type_compteur = "compteur_unites";
    listegroupe = unitesagrouper;
    liste_groupes = liste_groupes_unites;
  }
  if (objet === "dizaine") {
    liste_objets = liste_dizaines;
    type_compteur = "compteur_dizaines";
    listegroupe = dizainesagrouper;
    liste_groupes = liste_groupes_dizaines;
  }
  if (objet === "groupe_dizaines") {
    liste_objets = liste_groupes_dizaines;
    type_compteur = "compteur_dizaines";
    listegroupe = dizainesagrouper;
    liste_groupes = liste_groupes_dizaines;
  }
  if (objet === "centaine") {
    liste_objets = liste_centaines;
    type_compteur = "compteur_centaines";
    listegroupe = centainesagrouper;
    liste_groupes = liste_groupes_centaines;
  }
  if (objet === "groupe_centaines") {
    liste_objets = liste_groupes_centaines;
    type_compteur = "compteur_centaines";
    listegroupe = centainesagrouper;
    liste_groupes = liste_groupes_centaines;
  }
  if (objet === "millier") {
    liste_objets = liste_milliers;
    type_compteur = "compteur_milliers";
    liste_groupes = liste_groupes_milliers;
  }
  while (n < nombre) {

    let objetasupprimer;

    if (mode === 'auto') {
      objetasupprimer = liste_objets[liste_objets.length - 1];
    } else if (mode === 'casse') {
      objetasupprimer = dragged;
    } else if (mode === 'groupe') {
      objetasupprimer = listegroupe[n]
    }
    const index = liste_objets.indexOf(objetasupprimer);
    if (index > -1) {
      liste_objets.splice(index, 1);
    }

    objetasupprimer.remove()
    n++
  }
  maj_compteurs()
  maj_compteur_general()

}



function visibilite(liste_objets, compteur) {
  if (document.getElementById(compteur).style.display === 'inline') {
    document.getElementById(compteur).style.display = 'none';
  } else {
    document.getElementById(compteur).style.display = 'inline';
  }

}

let dragged;
let diffsourisx;
let diffsourisy;

// Clic de souris
function clic(event) {
  /* Si l'élément cliqué est une image */
  if (event.target.classList.contains('draggable')) {
    dragged = event.target;
    document.getElementById('zone_blocs').style.cursor = "url(images/grabbing.png) 18 19, grabbing";
    let rect;
    if (liste_milliers.includes(dragged)) {
      rect = document.getElementById('zone_milliers')
    }
    if (liste_centaines.includes(dragged) || liste_groupes_centaines.includes(dragged)) {
      rect = document.getElementById('zone_centaines')
    }
    if (liste_dizaines.includes(dragged) || liste_groupes_dizaines.includes(dragged)) {
      rect = document.getElementById('zone_dizaines')
    }
    if (liste_unites.includes(dragged) || liste_groupes_unites.includes(dragged)) {
      rect = document.getElementById('zone_unites')
    }

    // Il faut passer par targetTouches pour les écrans tactiles
    const clientX = event?.targetTouches?.[0]?.clientX || event?.clientX || 0;
    const clientY = event?.targetTouches?.[0]?.clientY || event?.clientY || 0;
    diffsourisx = clientX - rect.offsetLeft - dragged.offsetLeft;
    diffsourisy = clientY - rect.offsetTop - dragged.offsetTop;
    dragged.style.zIndex = hauteur + 1;
    posX_objet = dragged.offsetLeft;
    posY_objet = dragged.offsetTop;
    hauteur++
  }

}

var bouge = false;

// Déplacement de la souris
function move(event) {
  /* Si une image est en cours de déplacement */
  if (dragged) {
    /* Déplace l'image en fonction de la position de la souris */
    bouge = true;
    let rect
    if (liste_milliers.includes(dragged) || liste_groupes_milliers.includes(dragged)) {
      rect = document.getElementById('zone_milliers')
    }
    if (liste_centaines.includes(dragged) || liste_groupes_centaines.includes(dragged)) {
      rect = document.getElementById('zone_centaines')
    }
    if (liste_dizaines.includes(dragged) || liste_groupes_dizaines.includes(dragged)) {
      rect = document.getElementById('zone_dizaines')
    }
    if (liste_unites.includes(dragged) || liste_groupes_unites.includes(dragged)) {
      rect = document.getElementById('zone_unites')
    }

    // Il faut passer par targetTouches pour les écrans tactiles
    const pageX = event?.targetTouches?.[0]?.pageX || event?.pageX || 0;
    const pageY = event?.targetTouches?.[0]?.pageY || event?.pageY || 0;
    dragged.style.left = pageX - diffsourisx - rect.offsetLeft + "px";
    dragged.style.top = pageY - diffsourisy - rect.offsetTop + "px";
  }

}

// Relâchement de la souris
function release(event) {
  /* Si une image est en cours de déplacement */
  if (dragged && bouge == true) {
    /* Empêche le navigateur de faire défiler la page */
    event.preventDefault();
    verifie_relachement(dragged);

  }
  dragged = null;

  if (event.target.classList.contains('draggable')) {
    document.getElementById('zone_blocs').style.cursor = "url(images/grab.png) 19 19, grab";
  }

}

function verifie_groupement(x1, y1, x2, y2) {
  // Récupère toutes les images de la page
  var images = document.getElementsByTagName('img');
  unitesagrouper = [];
  dizainesagrouper = [];
  centainesagrouper = [];

  // Parcours toutes les images
  for (var i = 0; i < images.length; i++) {
    // Récupère les coordonnées de l'image
    var rect = images[i].getBoundingClientRect();
    var imageX1 = rect.left - 150;
    var imageY1 = rect.top - 100;
    var imageX2 = rect.right - 150;
    var imageY2 = rect.bottom - 100;
    // On inverse le sens du rectangle s'il est à l'envers.
    let x3;
    let y3;
    if (x1 > x2) { x3 = x1; x1 = x2; x2 = x3; }
    if (y1 > y2) { y3 = y1; y1 = y2; y2 = y3; }

    // Vérifie si l'image se trouve à l'intérieur des coordonnées données
    if (imageX2 >= x1 && imageY2 >= y1 && imageX1 <= x2 && imageY1 <= y2) {
      if (liste_centaines.includes(images[i])) { centainesagrouper.push(images[i]); }
      if (liste_dizaines.includes(images[i])) { dizainesagrouper.push(images[i]); }
      if (liste_unites.includes(images[i])) { unitesagrouper.push(images[i]); }
    }

  }

  if (fusion === false) {
    if (unitesagrouper.length >= 10) { supprime('unite', 10, 'groupe'); cree('zone_unites', 'groupe_unites', 'groupe'); }
    if (dizainesagrouper.length >= 10) { supprime('dizaine', 10, 'groupe'); cree('zone_dizaines', 'groupe_dizaines', 'groupe'); }
    if (centainesagrouper.length >= 10) { supprime('centaine', 10, 'groupe'); cree('zone_centaines', 'groupe_centaines', 'groupe'); }
  }
  else {
    if (unitesagrouper.length >= 10) { supprime('unite', 10, 'groupe'); cree('zone_dizaines', 'dizaine', 'groupe'); }
    if (dizainesagrouper.length >= 10) { supprime('dizaine', 10, 'groupe'); cree('zone_centaines', 'centaine', 'groupe'); }
    if (centainesagrouper.length >= 10) { supprime('centaine', 10, 'groupe'); cree('zone_milliers', 'millier', 'groupe'); }
  }

}



function verifie_relachement(dragged) {
  let zonerect;
  let objet;
  bouge = false
  if (liste_milliers.includes(dragged)) {
    zonerect = document.getElementById('zone_milliers');
    objet = 'millier';
  }
  else if (liste_groupes_centaines.includes(dragged)) {
    zonerect = document.getElementById('zone_centaines')
    objet = 'groupe_centaines'
  }
  else if (liste_centaines.includes(dragged)) {
    zonerect = document.getElementById('zone_centaines')
    objet = 'centaine'
  }
  else if (liste_groupes_dizaines.includes(dragged)) {
    zonerect = document.getElementById('zone_dizaines');
    objet = 'groupe_dizaines';
  }
  else if (liste_dizaines.includes(dragged)) {
    zonerect = document.getElementById('zone_dizaines');
    objet = 'dizaine';
  }
  else if (liste_groupes_unites.includes(dragged)) {
    zonerect = document.getElementById('zone_unites');
    objet = 'groupe_unites';
  }
  else if (liste_unites.includes(dragged)) {
    zonerect = document.getElementById('zone_unites');
    objet = 'unite';
  }
  if (fusion === true) {
    zonerect = document.getElementById('zone_globale');
    objet = 'unite';
  }


  const objectRect = dragged.getBoundingClientRect();
  const containerRect = zonerect.getBoundingClientRect();

  if (objectRect.left < containerRect.left ||
    objectRect.right > containerRect.right ||
    objectRect.top < containerRect.top ||
    objectRect.bottom > containerRect.bottom) {
    console.log('dehors')

    if (objet === 'unite') { retour(dragged); }

    if (objet === 'groupe_unites') {
      if (estdans(dragged, 'zone_dizaines')) { transforme(dragged, 'dizaine'); }
      else { retour(dragged); }
    }

    if (objet === 'dizaine') {
      if (estdans(dragged, 'zone_unites')) { casse_dizaine(dragged); }
      else { retour(dragged); }
    }

    if (objet === 'groupe_dizaines') {
      if (nb_col > 2 && estdans(dragged, 'zone_centaines')) { transforme(dragged, 'centaine'); }
      else { retour(dragged); }
    }

    if (objet === 'centaine') {
      if (estdans(dragged, 'zone_dizaines')) { casse_centaine(dragged); }
      else { retour(dragged); }
    }

    if (objet === 'groupe_centaines') {
      if (nb_col > 3 && estdans(dragged, 'zone_milliers')) { transforme(dragged, 'millier'); }
      else { retour(dragged); }
    }

    if (objet === 'millier') {
      if (estdans(dragged, 'zone_centaines')) { casse_millier(dragged); }
      else { retour(dragged); }
    }



  }



}


function maj_compteurs() {
  console.log(liste_unites.length + liste_groupes_unites.length * 10)
  document.getElementById('compteur_unites').innerHTML = liste_unites.length + (liste_groupes_unites.length * 10);
  document.getElementById('compteur_dizaines').innerHTML = liste_dizaines.length + (liste_groupes_dizaines.length * 10);
  if (nb_col > 2) { document.getElementById('compteur_centaines').innerHTML = liste_centaines.length + (liste_groupes_centaines.length * 10); }
  if (nb_col > 3) { document.getElementById('compteur_milliers').innerHTML = liste_milliers.length + (liste_groupes_milliers.length * 10); }
}

function transforme(objet, type) {
  let liste_groupes;
  let liste_objets;
  const zone_unites = document.getElementById('zone_unites');
  const zone_dizaines = document.getElementById('zone_dizaines');
  diffrectX = zone_unites.offsetLeft - zone_dizaines.offsetLeft;
  if (type === 'dizaine') { liste_groupes = liste_groupes_unites; liste_objets = liste_dizaines; document.getElementById('zone_dizaines').appendChild(objet); objet.style.left = objet.offsetLeft + diffrectX + "px"; }
  else if (type === 'centaine') { liste_groupes = liste_groupes_dizaines; liste_objets = liste_centaines; document.getElementById('zone_centaines').appendChild(objet); objet.style.left = objet.offsetLeft + diffrectX + "px" }
  else if (type === 'millier') { liste_groupes = liste_groupes_centaines; liste_objets = liste_milliers; document.getElementById('zone_milliers').appendChild(objet); objet.style.left = objet.offsetLeft + diffrectX + "px" }
  const index = liste_groupes.indexOf(objet);
  if (index > -1) { liste_groupes.splice(index, 1); }

  liste_objets.push(objet)
  maj_compteurs()
  maj_compteur_general()

}

function estdans(objet, zoneId) {
  var draggedRect = objet.getBoundingClientRect();
  var zone = document.getElementById(zoneId);
  var zoneRect = zone.getBoundingClientRect();
  return (draggedRect.left >= zoneRect.left &&
    draggedRect.right <= zoneRect.right &&
    draggedRect.top >= zoneRect.top &&
    draggedRect.bottom <= zoneRect.bottom);
}

function casse(event) {
  /* Si l'élément cliqué est une image */
  if (event.target.classList.contains('draggable')) {
    dragged = event.target;
    casse_action();
  }

}

async function casse_action() {

  let bouge = true;
  if (liste_groupes_centaines.includes(dragged) || liste_groupes_dizaines.includes(dragged) || liste_groupes_unites.includes(dragged)) { bouge = false; }
  if (fusion === false && bouge === true) {
    let positionx = dragged.offsetLeft;
    let positiony = dragged.offsetTop;
    let ciblex = document.getElementById('zone_unites').offsetWidth + positionx;
    let cibley = dragged.offsetTop;
    dragged.style.left = ciblex + "px";
  }
  if (liste_milliers.includes(dragged) || liste_groupes_centaines.includes(dragged)) { casse_millier(dragged) }
  if (liste_centaines.includes(dragged) || liste_groupes_dizaines.includes(dragged)) { casse_centaine(dragged) }
  if (liste_dizaines.includes(dragged) || liste_groupes_unites.includes(dragged)) { casse_dizaine(dragged) }
  dragged = null;

}









function casse_dizaine(dragged) {

  const zone_unites = document.getElementById('zone_unites');
  const zone_dizaines = document.getElementById('zone_dizaines');


  if (liste_groupes_unites.includes(dragged)) { diffrectX = 0 }
  else { diffrectX = zone_unites.offsetLeft - zone_dizaines.offsetLeft; }

  posXcasse = dragged.offsetLeft - diffrectX;
  posYcasse = dragged.offsetTop;

  for (let fois = 0; fois < 10; fois++) {
    cree('zone_unites', 'unite', 'casse');
  }

  for (let fois = 0; fois < 10; fois++) {
    position = position_casse('dizaine')

    deplace(liste_unites[liste_unites.length - 10 + fois], position[0], position[1])
  }
  rang_casse = 0
  if (liste_groupes_unites.includes(dragged)) { supprime('groupe_unites', 1, 'casse') }
  else { supprime('dizaine', 1, 'casse') }
}



function casse_centaine(dragged) {
  const zone_unites = document.getElementById('zone_unites');
  const zone_dizaines = document.getElementById('zone_dizaines');

  if (liste_groupes_dizaines.includes(dragged)) { diffrectX = 0 }
  else { diffrectX = zone_unites.offsetLeft - zone_dizaines.offsetLeft; }
  posXcasse = dragged.offsetLeft - diffrectX;
  posYcasse = dragged.offsetTop;
  for (let fois = 0; fois < 10; fois++) {
    cree('zone_dizaines', 'dizaine', 'casse');
  }
  for (let fois = 0; fois < 10; fois++) {
    position = position_casse('centaine')
    deplace(liste_dizaines[liste_dizaines.length - 10 + fois], position[0], position[1])
  }
  rang_casse = 0

  if (liste_groupes_dizaines.includes(dragged)) { supprime('groupe_dizaines', 1, 'casse') }
  else { supprime('centaine', 1, 'casse') }

}

function casse_millier(dragged) {
  const zone_unites = document.getElementById('zone_unites');
  const zone_dizaines = document.getElementById('zone_dizaines');

  if (liste_groupes_centaines.includes(dragged)) { diffrectX = 0 }
  else { diffrectX = zone_unites.offsetLeft - zone_dizaines.offsetLeft; }
  posXcasse = dragged.offsetLeft + dragged.offsetWidth - (0.7 * zone_centaines.offsetWidth / 2) - diffrectX;
  posYcasse = dragged.offsetTop;
  deplace(dragged, posXcasse, posYcasse)
  for (let fois = 0; fois < 10; fois++) {
    cree('zone_centaines', 'centaine', 'casse');
  }
  for (let fois = 0; fois < 10; fois++) {
    position = position_casse('millier')
    deplace(liste_centaines[liste_centaines.length - 10 + fois], position[0], position[1])
  }
  rang_casse = 0

  if (liste_groupes_centaines.includes(dragged)) { supprime('groupe_centaines', 1, 'casse') }
  else { supprime('millier', 1, 'casse') }

}

function deplace(image, ciblex, cibley) {
  deplacement = true;
  console.log('deplace')
  let startx = image.offsetLeft;
  let starty = image.offsetTop;
  let distance = Math.sqrt(Math.pow(ciblex - startx, 2) + Math.pow(cibley - starty, 2));
  let duration = distance * 2; // La durée de l'animation dépend de la distance à parcourir

  let startTime = null;
  function step(currentTime) {
    if (startTime === null) {
      startTime = currentTime;
    }
    let timeElapsed = currentTime - startTime;
    let fraction = timeElapsed / duration;
    if (fraction < 1) {
      let x = startx + (ciblex - startx) * fraction;
      let y = starty + (cibley - starty) * fraction;
      image.style.left = x + 'px';
      image.style.top = y + 'px';
      requestAnimationFrame(step);
    } else {
      image.style.left = ciblex + 'px';
      image.style.top = cibley + 'px';
    }
  }
  requestAnimationFrame(step);
  deplacement = false;
}


function verifie_compteurs() {
  affiche_compteur = true;
  let couleur_depassement;
  let couleur_normale;
  if (rideau_on === false) { couleur_normale = '#fff'; couleur_depassement = '#feb7b7'; } else { couleur_normale = '#000'; couleur_depassement = '#240f07'; }
  if (liste_unites.length > 9 || liste_groupes_unites.length > 0) { affiche_compteur = false; document.getElementById('zone_unites').style.backgroundColor = couleur_depassement } else { document.getElementById('zone_unites').style.backgroundColor = couleur_normale }
  if (nb_col > 2) {
    if (liste_dizaines.length > 9 || liste_groupes_dizaines.length > 0) { affiche_compteur = false; document.getElementById('zone_dizaines').style.backgroundColor = couleur_depassement } else { document.getElementById('zone_dizaines').style.backgroundColor = couleur_normale }
  }
  if (nb_col > 3) {
    if (liste_centaines.length > 9 || liste_groupes_centaines.length > 0) { affiche_compteur = false; document.getElementById('zone_centaines').style.backgroundColor = couleur_depassement } else { document.getElementById('zone_centaines').style.backgroundColor = couleur_normale }

  }
  if (affiche_compteur === true) {
    if (oeil === true) { document.getElementById('compteur_general').style.display = "block"; }
    document.getElementById('compteur').style.backgroundColor = "#bdffd5";
  }
  else {
    document.getElementById('compteur_general').style.display = "none";
    document.getElementById('compteur').style.backgroundColor = "orange";
  }

}

function retour(dragged) {
  // dragged.style.left = posX_objet + "px";
  // dragged.style.top = posY_objet + "px";
  deplace(dragged, posX_objet, posY_objet)
}



function cree_colonnes() {

  var radios = document.getElementsByName('colonnes');

  // Parcourir tous les boutons radio
  for (var i = 0; i < radios.length; i++) {
    // Vérifier si le bouton radio est sélectionné
    if (radios[i].checked) {
      // Récupérer la valeur du bouton radio sélectionné
      var nombre = radios[i].value;
      break;
    }
  }



  let checkbox = document.getElementById("fusionner");

  fusion = false;
  if (checkbox.checked) {
    fusion = true;
  }

  nb_col = nombre;

  if (fusion === true) {

    let div = document.createElement('div');
    let parentDiv = document.getElementById("zone_blocs");
    let larg_zone_bas = document.getElementById("zone_bas").offsetWidth;
    div.style.width = 'calc((100% - ' + (150 + 12) + 'px) /' + 1 + ')';
    let zone = 'zone_globale';
    div.id = zone;
    div.classList.add('colonne');
    div.classList.add('colonne_bas');
    parentDiv.prepend(div);
    let colonnes = document.querySelectorAll('colonne');
  }

  for (let fois = 0; fois < nombre; fois++) {



    let div = document.createElement('div');
    let parentDiv = document.getElementById("zone_blocs");
    let larg_zone_bas = document.getElementById("zone_bas").offsetWidth
    div.style.width = 'calc((100% - ' + (150 + 12 * nombre) + 'px) /' + nombre + ')';
    let zone = 'zone_' + liste_types[fois] + 's';
    div.id = zone;
    div.classList.add('colonne');
    div.classList.add('colonne_bas');
    parentDiv.prepend(div);
    let colonnes = document.querySelectorAll('colonne');

  //zone zoom
    div = document.createElement('div');
    div.style.width = 'calc((100% - ' + (150 + 12 * nombre) + 'px) /' + nombre + ')';
    div.id = zone+'_zoom';
    div.classList.add('colonne');
    div.classList.add('colonne_zoom');
    parentDiv.append(div);



    let divhaut = document.createElement('div');
    let parentDivhaut = document.getElementById("zone_haut");
    divhaut.style.width = 'calc((100% - ' + (150 + 12 * nombre) + 'px) /' + nombre + ')';
    let zonehaut = 'zone_haut_' + liste_types[fois] + 's';
    divhaut.id = zonehaut;
    divhaut.classList.add('colonne');
    parentDivhaut.prepend(divhaut);

    let divSource = document.getElementById('contenu_haut_' + liste_types[fois] + 's');
    let divCible = document.getElementById('zone_haut_' + liste_types[fois] + 's');
    divCible.innerHTML = divSource.innerHTML;

    let compteur = document.querySelector('.compteur');
    compteur.style.display = 'inline';
    

  }
}


function rideau() {
  let colonne_bas = document.getElementsByClassName("colonne_bas");
  let bloc = document.getElementsByClassName("draggable");

  if (rideau_on === false) {
    rideau_on = true;
    for (let j = 0; j < colonne_bas.length; j++) {
      colonne_bas[j].style.backgroundColor = "#000";

    }
    for (let j = 0; j < bloc.length; j++) {
      bloc[j].style.display = "none";
    }

  } else {
    rideau_on = false;
    for (let j = 0; j < colonne_bas.length; j++) {
      colonne_bas[j].style.backgroundColor = "#fff";
    }
    for (let j = 0; j < bloc.length; j++) {
      bloc[j].style.display = "inherit";
      verifie_compteurs()
    }
  }
}

function change_colonnes() {
  if (document.getElementById('compteur_general').innerHTML != '0') {
    reset_all();
  }
  const avirer = document.querySelectorAll('.colonne');
  avirer.forEach(element => {
    element.remove();
  });
  rideau_on = false;
  cree_colonnes();

}



function rangement(type_objet, liste_objets) {
  let nbx;
  let nby;
  let largeur_objet;
  let hauteur_objet;
  if (type_objet === 'unite') { nbx = 10; nby = 10; }
  if (type_objet === 'dizaine') { nbx = 2; nby = 10; }
  if (type_objet === 'centaine') { nbx = 2; nby = 5; }
  if (type_objet === 'millier') { nbx = 2; nby = 5; }
  for (let i = 0; i < liste_objets.length; i++) {
    let objet = liste_objets[i];
    largeur_objet = objet.offsetWidth;
    hauteur_objet = objet.offsetHeight;
    let position = position_rangement(i, largeur_objet, hauteur_objet, nbx, nby,type_objet);
    let dx = position[0];
    let dy = position[1];
    deplace(objet, dx, dy);
    objet.style.zIndex = hauteur + 1;
    hauteur++;
  }
}

function ranger_tout() {
  rangement('unite', liste_unites);
  rangement('dizaine', liste_dizaines);
  rangement('centaine', liste_centaines);
  rangement('millier', liste_milliers);
}

function melanger(type_objet, liste_objets) {
  let largeur_objet;
  let hauteur_objet;
  let largeur_zone;
  let hauteur_zone;
  let margex;
  let correctifx;
  let zone;
  if (type_objet === 'unite') { zone = document.getElementById('zone_unites') }
  else if (type_objet === 'dizaine') { zone = document.getElementById('zone_dizaines') }
  else if (type_objet === 'centaine') { zone = document.getElementById('zone_centaines') }
  else if (type_objet === 'millier') { zone = document.getElementById('zone_millliers') }


  if (fusion == false) {
    largeur_zone = document.getElementById('zone_unites').offsetWidth;
    hauteur_zone = document.getElementById('zone_unites').offsetHeight;
    margex = 5;
    correctifx = 0;
  } else {
    largeur_zone = document.getElementById('zone_globale').offsetWidth;
    hauteur_zone = document.getElementById('zone_globale').offsetHeight;
    margex = 155;
    correctifx = zone.offsetLeft;
  }

  for (let i = 0; i < liste_objets.length; i++) {
    let objet = liste_objets[i];
    largeur_objet = objet.offsetWidth;
    hauteur_objet = objet.offsetHeight;
    let dx = Math.floor(Math.random() * (largeur_zone - largeur_objet - 10)) - correctifx + margex;
    let dy = Math.floor(Math.random() * (hauteur_zone - hauteur_objet - 10)) + 5;
    deplace(objet, dx, dy);
    objet.style.zIndex = hauteur + 1;
    hauteur++;
  }
}




function melanger_tout() {
  melanger('unite', liste_unites);
  melanger('dizaine', liste_dizaines);
  melanger('centaine', liste_centaines);
  melanger('milliers', liste_milliers);
}


function position_rangement(rang, largeur_objet, hauteur_objet, nbx, nby,type_objet) {

  let nbz;
  let largeur_zone;
  let hauteur_zone;
  let rangx;
  let rangy;
  let posx;
  let posy;
  let ecart_x;
  let ecart_y;
  let depart_x;
  let depart_y;

  largeur_zone = document.getElementById('zone_unites').offsetWidth;
  hauteur_zone = document.getElementById('zone_unites').offsetHeight;
  ecart_x = (largeur_zone - nbx * largeur_objet) / (nbx + 1);
  ecart_y = (hauteur_zone - nby * hauteur_objet) / (nby + 1);
  if (ecart_x < 0) { depart_x = 0; ecart_x = (largeur_zone - nbx * largeur_objet) / (nbx - 1); } else { depart_x = ecart_x; }
  if (ecart_y < 0) { depart_y = 0; ecart_y = (hauteur_zone - nby * hauteur_objet) / (nby - 1); } else { depart_y = ecart_y; }

  nbz = nbx * nby;

  rangy = Math.trunc(rang / nbx);
  rangx = rang - (nbx * Math.trunc(rang / nbx));

  posx = depart_x + ecart_x * rangx + largeur_objet * rangx;
  posy = depart_y + ecart_y * rangy + hauteur_objet * rangy;

  if (rang + 1 > nbz) { posx = Math.floor(Math.random() * (largeur_zone - largeur_objet - 10)) + 5; posy = Math.floor(Math.random() * (hauteur_zone - hauteur_objet - 10)) + 5 }
  if (type_objet==='centaine' && rangy% 2!=0) {
    posx = posx + 0.7*ecart_x;
  }


  let position = [posx, posy];

  return position;

}

function hasard(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}


function tirage() {
  let min_unites = document.getElementById("min_unites").value;
  let max_unites = document.getElementById("max_unites").value;
  let min_dizaines = document.getElementById("min_dizaines").value;
  let max_dizaines = document.getElementById("max_dizaines").value;
  let min_centaines = document.getElementById("min_centaines").value;
  let max_centaines = document.getElementById("max_centaines").value;
  let min_milliers = document.getElementById("min_milliers").value;
  let max_milliers = document.getElementById("max_milliers").value;
  console.log('minunites ' + min_unites + ' maxunites ' + max_unites)

  let u = hasard(min_unites, max_unites);
  let d = hasard(min_dizaines, max_dizaines);
  let c = hasard(min_centaines, max_centaines);
  let m = hasard(min_milliers, max_milliers);
  console.log('u ' + u + ' d ' + d)
  reset_all();
  document.getElementById('compteur_general').style.display = 'none';
  document.getElementById('interrupteur_general').style.backgroundImage = 'url(images/oeil_inactif.png)';
  oeil = false;

  var elements = document.getElementsByClassName("compteur");
  for (var i = 0; i < elements.length; i++) {
    elements[i].style.display = "none";
  }

  for (let fois = 0; fois < u; fois++) { cree('zone_unites', 'unite', 'auto'); }
  for (let fois = 0; fois < d; fois++) { cree('zone_dizaines', 'dizaine', 'auto'); }
  if (nb_col > 2) {
    for (let fois = 0; fois < c; fois++) { cree('zone_centaines', 'centaine', 'auto'); }
  }
  if (nb_col > 3) {
    for (let fois = 0; fois < m; fois++) { cree('zone_milliers', 'millier', 'auto'); }
  }
}

function verifie_nombre() {
  let nombre = document.getElementById("nombre_choix").value;
  let regex = /^[0-9]{0,4}$/;

  if (!regex.test(nombre)) {
    // Efface le dernier caractère entré si l'utilisateur entre plus de quatre chiffres
    document.getElementById("nombre_choix").value = nombre.slice(0, 4);
  } else {
    // Efface les caractères qui ne sont pas des chiffres
    document.getElementById("nombre_choix").value = nombre.replace(/[^0-9]/g, '');
  }
}



function cree_nombre() {
  reset_all();
  let nombre = document.getElementById("nombre_choix").value;
  console.log(nombre)
  let u;
  let d;
  let c;
  let m;

  if (nb_col > 3) { m = Math.floor(nombre / 1000); nombre = nombre - (1000 * m) }
  if (nb_col > 2) { c = Math.floor(nombre / 100); nombre = nombre - (100 * c) }
  d = Math.floor(nombre / 10);
  nombre = nombre - (10 * d);
  u = nombre;
  console.log('nombre ' + nombre + ' m ' + m + ' c ' + c + ' d ' + d + ' u ' + u);

  for (let fois = 0; fois < u; fois++) { cree('zone_unites', 'unite', 'auto'); }
  for (let fois = 0; fois < d; fois++) { cree('zone_dizaines', 'dizaine', 'auto'); }
  if (nb_col > 2) { for (let fois = 0; fois < c; fois++) { cree('zone_centaines', 'centaine', 'auto'); } }
  if (nb_col > 3) { for (let fois = 0; fois < m; fois++) { cree('zone_milliers', 'millier', 'auto'); } }
}


const canvas = document.getElementById("canvas");
const rectangle_selection = canvas.getContext("2d");

let startX, startY;

/* Je récupère les coordonnées de départ au clic de la souris. */
function debut_rectangle(event){
  const clientX = event?.targetTouches?.[0]?.clientX || event?.clientX || 0;
  const clientY = event?.targetTouches?.[0]?.clientY || event?.clientY || 0;
  const rectangle = canvas.getBoundingClientRect();
  startX = clientX - rectangle.left;
  startY = clientY - rectangle.top;
  canvas.style.zIndex = hauteur + 1
}



/* J'efface le rectangle et je le refais à chaque déplacement de souris */
function trace_rectangle(event){
  if (startX != null && startY != null) {
    const clientX = event?.targetTouches?.[0]?.clientX || event?.clientX || 0;
    const clientY = event?.targetTouches?.[0]?.clientY || event?.clientY || 0;
    const rectangle = canvas.getBoundingClientRect();
    width = clientX - rectangle.left - startX;
    height = clientY - rectangle.top - startY;
    rectangle_selection.clearRect(0, 0, canvas.width, canvas.height);
    rectangle_selection.fillStyle = "rgba(0, 0, 255, 0.5)";
    rectangle_selection.fillRect(startX, startY, width, height);

  }
}


/* J'efface le rectangle au relâchement de la souris. */
function fin_rectangle(event) {
  rectangle_selection.clearRect(0, 0, canvas.width, canvas.height);
  verifie_groupement(startX, startY, startX + width, startY + height)
  startX = null;
  startY = null;
  canvas.style.zIndex = 1

}

function info() {
  document.getElementById('apropos').style.display = 'inherit';
  document.getElementById('darkbox').style.display = 'inherit';
}

function info_ferme() {
  document.getElementById('apropos').style.display = 'none';
  document.getElementById('darkbox').style.display = 'none';
}

function aide() { }

cree_colonnes(2);

document.addEventListener("touchstart", clic);
document.addEventListener("touchmove", move);
document.addEventListener("touchend", release);

document.addEventListener("mousedown", clic);
document.addEventListener("mousemove", move);
document.addEventListener("mouseup", release);

document.addEventListener("dblclick", casse);
canvas.addEventListener("mousedown", debut_rectangle);
canvas.addEventListener("touchstart", debut_rectangle);
canvas.addEventListener("mousemove", trace_rectangle);
canvas.addEventListener("touchmove", trace_rectangle);
canvas.addEventListener("mouseup", fin_rectangle);
canvas.addEventListener("touchend", fin_rectangle);

// Raccourcis claviers
document.addEventListener("keydown", function (event) {
  if (event.key === "u" || event.key === "U") { cree('zone_unites', 'unite', 'auto'); }
  if (event.key === "d" || event.key === "D") { cree('zone_dizaines', 'dizaine', 'auto'); }
  if ((event.key === "c" || event.key === "C") && nb_col > 2) { cree('zone_centaines', 'centaine', 'auto'); }
  if ((event.key === "m" || event.key === "M") && nb_col > 3) { cree('zone_milliers', 'millier', 'auto'); }
  if (event.key === "t" || event.key === "T") { tirage(); }
});
const nombre_choix = document.getElementById("nombre_choix");

nombre_choix.addEventListener("keydown", function (event) {
  if (event.key === "t" || event.key === "Enter") { cree_nombre(); }
});


